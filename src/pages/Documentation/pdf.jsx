import React from "react";
import { Page, Text, View, Document, StyleSheet, PDFViewer } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
  },
  title: {
    fontSize: "20pt",
    textAlign: "center",
  },
  section: {
    margin: 25,
    padding: 10,
    flexGrow: 1,
  },
  right: {
    textAlign: "right",
  },
  margin: {
    marginTop: 50,
    textAlign: "right",
  },
});

const MyDocument = () => {
  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.title}>Cara permainan</Text>
          <View style={styles.section}>
            <Text>Terdapat tiga isyarat tangan dalam permainan ini.</Text>
            <Text>Batu digambarkan oleh tangan mengepal,</Text>
            <Text>Gunting digambarkan oleh jari telunjuk dan tengah,</Text>
            <Text>Kertas digambarkan oleh tangan terbuka.</Text>
            <Text>Tujuan dari permainan adalah mengalahkan lawan bermain.</Text>
            <Text>Aturan standar adalah batu mengalahkan Gunting,</Text>
            <Text>Gunting mengalahkan Kertas,</Text>
            <Text>dan Batu mengalahkan kertas</Text>
            <Text>.</Text>
            <Text>Jika kedua pemain mengeluarkan isyarat yang sama,</Text>
            <Text>maka permainan diulang. Kadang kala pemain menggunakan sistem berulang-ulang</Text>
            <Text>artinya sekali kemenangan tidak cukup untuk menghentikan permainan.</Text>
            <Text>Misalnya pemain yang menang 5 kali terlebih dahulu menjadi pemenang.</Text>
          </View>
        </View>
      </Page>
    </Document>
  );
};

export default function Pdf() {
  return (
    <PDFViewer className="col-10 mb-5 mx-auto shadow justify-content-center d-flex" height={600}>
      <MyDocument />
    </PDFViewer>
  );
}
