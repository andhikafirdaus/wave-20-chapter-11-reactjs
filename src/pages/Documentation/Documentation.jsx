import React, { useState } from "react";
import { ControlBar, Player } from "video-react";
import "video-react/dist/video-react.css";
import { Button } from "reactstrap";
import PDF from "./pdf";

const sources = {
  sintelTrailer: "https://media.w3.org/2010/05/sintel/trailer.mp4",
  bunnyTrailer: "https://media.w3.org/2010/05/bunny/trailer.mp4",
  bunnyMovie: "https://www.w3schools.com/html/mov_bbb.mp4",
  test: "https://media.w3.org/2010/05/video/movie_300.webm",
};

export default function Documentation() {
  const [player, setPlayer] = useState();
  const [source, setSource] = useState(sources.bunnyTrailer);

  const play = () => {
    player.play();
  };

  const pause = () => {
    player.pause();
  };

  const load = () => {
    player.load();
  };

  return (
    <div>
      <h1 className="ms-3 mt-5">Strategi Permainan</h1>
      <Player
        ref={(player) => {
          setPlayer(player);
        }}
      >
        <source src={source}></source>
        <ControlBar autoHide></ControlBar>
      </Player>
      <div className="py-3">
        <Button onClick={play} className="ms-3">
          Play
        </Button>
        <Button onClick={pause} className="ms-3">
          Pause
        </Button>
      </div>
      <div className="py-3"></div>
      <h1 className="ms-3 mt-5">Aturan Permainan</h1>
      <div className="col mx-auto">
        <PDF/>
      </div>
    </div>
  );
}
